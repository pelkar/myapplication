package com.mobi.lab.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mobi.lab.veriff.data.Veriff;
import mobi.lab.veriff.util.LanguageUtil;
import mobi.lab.veriff.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_VERIFF = 8000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String baseUrl = "https://sandbox.veriff.me";

        final String sessionToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3Rva2VuIjoiODI3NTQxODctM2VjYS00NGNiLThjMGUtN2M5YzQxZmQ4NmI2Iiwic2Vzc2lvbl9pZCI6IjRlODUwMTkxLTE4N2EtNDNlMC1iM2Y3LWFkOGM1NWIyZWE1NyIsImlhdCI6MTUwODQ4OTg1MSwiZXhwIjoxNTA5MDk0NjUxfQ.pvMv_sPsSiLwBbAa4U6zkMLZ_J54Spwov4K_qgWLxtc";

        //enable logging for the library
        Veriff.setLoggingImplementation(Log.getInstance(MainActivity.class));

        // Retrieves the item index and then passes on it's code
        String code = LanguageUtil.getSupportedLanguageCodes()[1];
        Veriff.Builder veriffSDK = new Veriff.Builder(baseUrl, sessionToken);
        veriffSDK.controlsColor(Color.RED);
        veriffSDK.launch(MainActivity.this, REQUEST_VERIFF, code);
    }
}
